class ModificationsController < ApplicationController
    
  def index
    @modifications = Modification.order('created_at')
    @modifications = Modification.paginate(page: params[:page])
  end

  def new
    @modifications = Modification.new
  end

  def create
    @modifications = Modification.new(modification_params)
    if @modifications.save
      flash[:success] = "Modification contributed!"
      redirect_to modifications_path
    else
      render 'new'
    end
  end
  
  def destroy
    @modifications = Modification.find(params[:id])
    @modifications.destroy
    flash[:danger] = "Contribution removed!"
    redirect_to modifications_path
  end

  private

  def modification_params
    params.require(:modification).permit(:image, :title)
  end
  
end