class Users::RegistrationsController < Devise::RegistrationsController
   
    before_filter :select_plan, only: :new
   
    def create
        super do |resource|
            if params[:plan]
                resource.plan_id = params[:plan]
                if resource.plan_id == 1
                    resource.save
                elsif resource.plan_id == 2
                    resource.save_with_payment
                elsif resource.plan_id == 3
                    resource.save
                elsif resource.plan_id == 4
                    resource.save_with_payment
                elsif resource.plan_id == 5
                    resource.save
                elsif resource.plan_id == 6
                    resource.save_with_payment
                elsif resource.plan_id == 7
                    resource.save
                elsif resource.plan_id == 8
                    resource.save_with_payment
                end
            end
        end
    end
    
    private
        def select_plan
            @plan = Plan.find_by(:id => params[:plan])
            if(!@plan)
              flash[:success] = "Please select a membership plan to register."
              redirect_to home_path
            end
        end
end