class PagesController < ApplicationController
    before_action :authenticate_user!, except:  [:landing, :plans]
 
    def landing
        
    end
    
    def plans
    end
 
    def home
        @contributor_plan = Plan.find(1)
        @elitecontributor_plan = Plan.find(2)
        @technician_plan = Plan.find(3)
        @elitetechnician_plan = Plan.find(4)
        @center_plan = Plan.find(5)
        @elitecenter_plan = Plan.find(6)
        @affair_plan = Plan.find(7)
        @eliteaffair_plan = Plan.find(8)
    end
    
    def about
    end
    
    def pulse
        @users = User.all
    end
    
    def collection
    end
    
    def market
    end
    
    def affair
        @affair_plan = Plan.find(7)
        @eliteaffair_plan = Plan.find(8)
    end
    
    def time
    end
    
    def past
    end
    
    def present
    end
    
    def future
    end
    
end