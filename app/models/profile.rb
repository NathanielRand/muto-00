class Profile < ActiveRecord::Base
    belongs_to :user
    validates :first_name, :last_name, :location, :modifications, presence: true

    has_attached_file :avatar, 
        styles: { 
            medium: "500x500>", 
            thumb: "100x100>"}, 
        default_url: "/images/:style/missing.png"
    validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
    
end