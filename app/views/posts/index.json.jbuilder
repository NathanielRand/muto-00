json.array!(@posts) do |post|
  json.extract! post, :id, :position, :location, :center, :wage, :description
  json.url post_url(post, format: :json)
end
