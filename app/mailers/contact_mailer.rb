class ContactMailer < ActionMailer::Base

    default to: 'mutoworldcontact@gmail.com'

    def contact_email(name, email, body)
        @name = name
        @email = email
        @body = body
        
        mail(from: email, subject: 'MUTO Contact Form Message')
    end
end