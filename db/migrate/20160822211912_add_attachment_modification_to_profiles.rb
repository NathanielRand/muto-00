class AddAttachmentModificationToProfiles < ActiveRecord::Migration
  def self.up
    change_table :profiles do |t|
      t.attachment :modification
    end
  end

  def self.down
    remove_attachment :profiles, :modification
  end
end
