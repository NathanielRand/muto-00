class AddAttachmentImageToModifications < ActiveRecord::Migration
  def self.up
    change_table :modifications do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :modifications, :image
  end
end
