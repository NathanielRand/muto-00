class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :position
      t.string :location
      t.string :center
      t.string :wage
      t.text :description

      t.timestamps null: false
    end
  end
end
